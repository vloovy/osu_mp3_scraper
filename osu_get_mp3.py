import os
import re
import shutil


def get_paths():
    """
    :return: two strings: path to the folder where we copy songs from 
             and path to the folder where we copy songs to.
    """
    return (input("Please enter your osu! song folder e.g. C:\osu!\Songs \n"),
            input("Please enter folder where you want to copy you mp3s e.g. C:\music \n"))


def correct_file(file):
    """
    Check if the file is of a correct name and format
    :param file: name of a file the function checks
    :return: True if the files name ends with .mp3 and doesn't start with "normal",
             "soft", "drum", "sectionpass", "sectionfail", else it returns False
    """
    return (str(file).endswith(".mp3") and not str(file).startswith("normal")
            and not str(file).startswith("soft") and not str(file).startswith("drum")
            and not str(file).startswith("sectionpass") and not str(file).startswith("sectionfail"))


def get_new_mp3_name(filepath):
    """
    Get new name for mp3 file
    :param filepath: path (source) of the mp3 file
    :return: new name based on the folder the mp3 file was in, without the map id
    """
    name = filepath.split("\\")[-2]
    return re.sub(r"(\d+\s)", "", name)+".mp3"


def main():
    """
    Get paths of all suitable mp3 files, copy them to a new folder and rename them
    :raises: FileExistsError if we try to copy and rename more than one mp3 from one folder
    :return: 
    """
    songs_directory, copy_destination = get_paths()
    filepaths = []

    for path, _, files in os.walk(songs_directory):
        filepaths.extend([os.path.join(path, file) for file in files if correct_file(file)])

    for filepath in filepaths:
        old_mp3_name = filepath.split("\\")[-1]
        new_mp3_name = get_new_mp3_name(filepath)
        try:
            shutil.copy(filepath, copy_destination)
            os.chdir(copy_destination)
            os.rename(old_mp3_name, new_mp3_name)
        except FileExistsError:
            print(filepath + " was not copied.")


if __name__ == "__main__":
    main()
